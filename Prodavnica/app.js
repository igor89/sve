angular.module("angularShop",[])
.controller("mainController", function mainController(){
    this.proizvodi= [
        {
            naziv: "Tefal Pegla",
            slika: "img/slika1.jpg",
            cena: 6190
        },
        {
            naziv: "Gorenje Mikser",
            slika: "img/slika2.jpg",
            cena: 4690
        },
        {
            naziv: "Gorenje Usisivac",
            slika: "img/slika3.jpg",
            cena: 9990
        },
        {
            naziv: "Delimano Stapni Mikser",
            slika: "img/slika4.jpg",
            cena: 5990
        },
        {
            naziv: "Esperanza Elektricna Salamoreznica",
            slika: "img/slika5.jpg",
            cena: 5690
        },
        {
            naziv: "Delimano Nutribulet",
            slika: "img/slika6.jpg",
            cena: 15190
        }
    ];

});
