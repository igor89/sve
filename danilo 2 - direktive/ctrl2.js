angular.module('zadatak1')
    .directive('dirTwo', function () {
        return {
            templateUrl: 'ctrl2.html',
            scope: {
                ja: '=',
                send: '&onSend'
            },
            restrict: 'E'
        }
    });
