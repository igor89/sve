angular.module('zadatak1')
    .directive('dirOne', function () {
        this.a = 0;
        this.b = 0;
        return {
            templateUrl: 'ctrl1.html',
            restrict: 'E',
            scope: {
                ime: '=ime',
                prezime: '=',
                mesto: '=',
                galerija: '=',
                add: '&onAdd'
            }
        }
    });
