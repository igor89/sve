angular.module(`zadatak1`, [])
    .controller('KursCtrl',
        function KursCtrl($scope) {
            //#region Variable
            $scope.ime = 'Danilo';
            $scope.prezime = 'Mogin';
            $scope.mesto = 'Novi Sad';
            $scope.a = 0;
            $scope.b = 0;
            $scope.galerija = [
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                },
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                },
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                },
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                }
            ];
            $scope.ja = {
                ime: 'Danilo',
                prezime: 'Mogin',
                godina: 33,
                email: 'danilo.mogin@gmail.com',
            }
            //#endregion
            $scope.send = (ime, prezime, godina, email) => {
                var userObj = {
                    ime,
                    prezime,
                    godina,
                    email,
                };
                console.log(`userObj`);
                console.log(userObj);
            }
            $scope.add = (a, b) => {
                console.log(`a + b`);
                console.log(a + b);

                return a + b;
            }
        }
    );
