angular.module('myApp', [])
    .controller('MainControler', function ($scope) {
        $scope.checkbox = true;
        $scope.zaradaSakrij = true;
        $scope.osobe = [
            {
                ime: "Marko",
                datumRodj: "1980-11-22T23:00:00.00Z",
                pol: " Muški",
                zarada: "55000.778",
                grad: "Novi Sad"
            },
            {
                ime: "Sara",
                datumRodj: "1970-05-04T23:00:00.00Z",
                pol: "Ženski",
                zarada: "68000",
                grad: "Novi Sad"
            },
            {
                ime: "Janko",
                datumRodj: "1974-08-14T23:00:00.00Z",
                pol: " Muški",
                zarada: "57000",
                grad: "Beograd"
            },
            {
                ime: "Ana",
                datumRodj: "1979-10-26T23:00:00.00Z",
                pol: "Ženski",
                zarada: "53000",
                grad: "Subotica"
            },
            {
                ime: "Todor",
                datumRodj: "1983-12-29T23:00:00.00Z",
                pol: " Muški",
                zarada: "60000",
                grad: "Beograd"
            },
        ]

        $scope.osobe2 = [
            {
                ime: "Petar",
                pol: "Muski",
                zarada: 55000
            },
            {
                ime: "Sara",
                pol: "Zenski",
                zarada: 68000
            },
            {
                ime: "Marko",
                pol: "Muski",
                zarada: 57000
            },
            {
                ime: "Pera",
                pol: "Muski",
                zarada: 53000
            },
            {
                ime: "Todor",
                pol: "Muski",
                zarada: 60000
            },
        ]

        $scope.column = 'pol';
        $scope.reverse = false;

        $scope.sortColumn = function (col) {
            $scope.column = col;
            if ($scope.reverse) {
                $scope.reverse = false;
                //$scope.reverseclass = 'arrow-up';
            } else {
                $scope.reverse = true;
                //$scope.reverseclass = 'arrow-down';
            }
        };

        $scope.sortClass = function (col) {
            if ($scope.column == col) {
                if ($scope.reverse) {
                    return 'arrow-down';
                } else {
                    return 'arrow-up';
                }
            } else {
                return '';
            }
        }
    })