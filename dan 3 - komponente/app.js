angular.module("mojAngular",[])
.controller("firstController", function firstController(){
    
    this.ja = {
        ime:"Igor",
        prezime:"Marcikic",
        mesto:"Kovilj",
        dan:"Nedelja"
    };

    this.objekti = [
        {
            slika: "https://cdn1.medicalnewstoday.com/content/images/articles/322/322868/golden-retriever-puppy.jpg",
            naziv: "Slika 1"       
        },
        {
            slika: "https://www.cesarsway.com/sites/newcesarsway/files/styles/large_article_preview/public/Common-dog-behaviors-explained.jpg?itok=FSzwbBoi",
            naziv: "Slika 2" 
        },
        {
            slika: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13002253/GettyImages-521536928-_1_.jpg",
            naziv: "Slika 3" 
        }
    ];

    this.a = 0;
    this.b = 0;
    
    this.add = (a, b) => {
        return a + b;
    }
    
});
