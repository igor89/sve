angular
.module("mojAngular")
.component("secondComponent", {
    templateUrl:"template2.html",
    bindings: {
        a: "<",
        b: "<",
        onAdd: "&"
    },
 
    controller: function CtrlOneController() {
        
        this.add = function (a, b) {
            console.log(a+b);
        }
    }
});