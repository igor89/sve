angular.module('myApp', [])

.controller('PrviKontroler', function($scope){
    $scope.ime= 'Igor';
    $scope.prezime= 'Marcikic';
    $scope.mesto= 'Kovilj';

    $scope.slike = [
        {
            naziv: 'Slika 1',
            src: 'https://www.catster.com/wp-content/uploads/2018/07/Savannah-cat-long-body-shot.jpg'
        },
        {
            naziv: 'Slika 2',
            src: 'https://media.mnn.com/assets/images/2018/07/cat_eating_fancy_ice_cream.jpg.838x0_q80.jpg'
        },
        {
            naziv: 'Slika 3',
            src: 'https://www.petmd.com/sites/default/files/what-does-it-mean-when-cat-wags-tail.jpg'
        }
    ];
    
    $scope.ja = {
        ime: 'Igor',
        prezime: 'Marcikic',
        godine: 30,
        email: 'marcikicigor@gmail.com' 
    }

    $scope.jalog = () => {
        console.log($scope.ja);
    }  

})
