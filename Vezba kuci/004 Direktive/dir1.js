angular.module('myApp')
.directive('dirOne', function(){
    return {
        restrict: 'AE',
        templateUrl: 'temp1.html',
        scope: {
            ja: '=',
            jalog: '&onJalog'
        }
    }
})