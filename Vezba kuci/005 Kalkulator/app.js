angular.module('myApp',[])
.controller('CalculatorController', function($scope, Sabiranje, Oduzimanje){
    $scope.num1 = 0;
    $scope.num2 = 0;

    $scope.sabiranje = function(){
        $scope.result = Sabiranje.rezultat($scope.num1, $scope.num2);
    }

    $scope.oduzimanje = function(){
        $scope.result = Oduzimanje.rezultat($scope.num1, $scope.num2);
    }

})