angular.module('myApp')
.service('Sabiranje', function () {
    this.rezultat = function (a, b) {
        return a + b;
    }
})
.service('Oduzimanje', function () {
    this.rezultat = function (a, b) {
        return a - b;
    }
})