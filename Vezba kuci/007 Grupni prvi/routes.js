angular.module('myApp')
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
            .when('/home', {
                template: '<home-komponenta></home-komponenta>',
            })
            .when('/galerija', {
                template: '<galerija-komponenta>',
            })
            .when('/digitron', {
                template: '<digitron-direktiva ></digitron-direktiva>',
            })
            .otherwise({
                redirectTo: '/'
            })

    });