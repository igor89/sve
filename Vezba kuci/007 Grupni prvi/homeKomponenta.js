app.component('homeKomponenta', {
    templateUrl: 'home.html',
    controller: function($scope, Logovanje) {
        $scope.korisnik = {
            ime: "Pera",
            prezime: "Peric",
            mesto: "Novi Sad",
            godine: 30
        }
        $scope.log = function(){
            this.log = Logovanje.log($scope.korisnik);
        }
    }
})