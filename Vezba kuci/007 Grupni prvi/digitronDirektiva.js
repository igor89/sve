app.directive('digitronDirektiva', function(){
    return {
        restrict: 'AE',
        templateUrl: 'digitron.html',
        controller: function($scope, Sabiranje, Oduzimanje, Mnozenje, Deljenje) {
            $scope.num1 = 0;
            $scope.num2 = 0;
            $scope.digitron = function(){
                $scope.sabiranje = Sabiranje.rezultat($scope.num1, $scope.num2);
                $scope.oduzimanje = Oduzimanje.rezultat($scope.num1, $scope.num2);
                $scope.mnozenje = Mnozenje.rezultat($scope.num1, $scope.num2);
                $scope.deljenje = Deljenje.rezultat($scope.num1, $scope.num2);
                }
        }
    }
})