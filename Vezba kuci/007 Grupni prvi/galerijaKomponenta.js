app.component('galerijaKomponenta', {
    templateUrl: 'galerija.html',
    controller: function($scope) {
        $scope.galerija = [
            {
                ime: "Prva slika",
                src: "https://www.akc.org/wp-content/themes/akc/component-library/assets/img/welcome.jpg",
                opis: "Ovo je opis prve slike"
            },
            {
                ime: "Druga slika",
                src: "https://www.akc.org/wp-content/themes/akc/component-library/assets/img/welcome.jpg",
                opis: "Ovo je opis druge slike"
            },
            {
                ime: "Treca slika",
                src: "https://www.akc.org/wp-content/themes/akc/component-library/assets/img/welcome.jpg",
                opis: "Ovo je opis trece slike"
            },
        ]
        
        }

    })