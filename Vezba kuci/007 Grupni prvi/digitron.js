app.service('Sabiranje', function () {
    this.rezultat = function (a, b) {
        return a + b;
    }
})
.service('Oduzimanje', function () {
    this.rezultat = function (a, b) {
        return a - b;
    }
})
.service('Mnozenje', function () {
    this.rezultat = function (a, b) {
        return a * b;
    }
})
.service('Deljenje', function () {
    this.rezultat = function (a, b) {
        return a / b;
    }
})
