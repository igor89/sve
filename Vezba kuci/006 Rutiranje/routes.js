angular.module('myApp')
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
            .when('/a', {
                templateUrl: './a/a.dogs.html',
                controller: 'ADogsController'
            })
            .when('/b', {
                templateUrl: './b/b.dogs.html',
                controller: 'BDogsController'
            })
            .when('/c', {
                templateUrl: './c/c.dogs.html',
                controller: 'CDogsController'
            })
            .when('/a/:dogId', { // : oznacava da je to parametar
                template: '<dogs-component dogs="dogs"></dogs-component>'
            })
            .when('/a/:dogId', { // : oznacava da je to parametar
                template: '<alascan-malamute></alascan-malamute>'
            })
            .when('/b/:dogId', { // : oznacava da je to parametar
                template: '<barbet></barbet>'
            })
            .when('/b/:dogId', { // : oznacava da je to parametar
                template: '<basenji></basenji>'
            })
            .when('/c/:dogId', { // : oznacava da je to parametar
                template: '<canaan></canaan>'
            })
            .when('/c/:dogId', { // : oznacava da je to parametar
                template: '<cane-corso></cane-corso>'
            })
            .otherwise({
                redirectTo: '/'
            })

    });