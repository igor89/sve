angular.module('myApp')
    .component('basenji', {
        templateUrl: './b/basenji.html', // tu gore je $ctrl jer smo dole u funkciji stavili this
        controller: ['$routeParams',  // uglaste zagrade pokazu sta ima u funkciji, i za minifikaciju
            function PhoneDetailsController($routeParams) {
                this.dogId = $routeParams.dogId; // phoneId je taj parametar koji se pozivau routes

            }
        ]
    })