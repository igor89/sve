angular.module('myApp')
.controller('bDogsController', function($scope,$routeParams){
    $scope.dogId = $routeParams.dogId;
    $scope.title= 'Dogs Starting With The Letter "B"';
    $scope.dogs = [
        {
            id: 0,
            name: "Barbet",
            snippet: "An archetypic water dog of France, the Barbet is a rustic breed of medium size and balanced proportions who appears in artwork as early as the 16th century. In profile, the Barbet is slightly rectangular with a substantial head and long, sweeping tail. He has a long, dense covering of curly hair and a distinctive beard. An agile athlete, the Barbet has been used primarily to locate, flush, and retrieve birds. He has a cheerful disposition and is very social and loyal.",
            src: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/27114851/barbet.lying_.jpg"
        },
        {
            id: 1,
            name: "Basenji",
            snippet: "The Basenji, Africa’s “Barkless Dog,” is a compact, sweet-faced hunter of intelligence and poise. They are unique and beguiling pets, best for owners who can meet their exercise needs and the challenge of training this catlike canine.",
            src: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001133/Basenji-On-White-01.jpg"
        }
    ];
    
})


