angular.module('myApp')
.controller('aDogsController', function($scope,$routeParams){
    $scope.dogId = $routeParams.dogId;
    $scope.title= 'Dogs Starting With The Letter "A"';
    $scope.dogs = [
        {
            id: 0,
            name: "Afghan Hound",
            snippet: "Among the most eye-catching of all dog breeds, the Afghan Hound is an aloof and dignified aristocrat of sublime beauty. Despite his regal appearance, the Afghan can exhibit an endearing streak of silliness and a profound loyalty.",
            src: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/19145725/Afghan-Hound1.jpg"
        },
        {
            id: 1,
            name: "Alaskan Malamute",
            snippet: "An immensely strong, heavy-duty worker of spitz type, the Alaskan Malamute is an affectionate, loyal, and playful but dignified dog recognizable by his well-furred plumed tail carried over the back, erect ears, and substantial bone.",
            src: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/13001815/Alaskan-Malamute-On-White-03.jpg"
        }
    ];
    
})


