angular.module('myApp')
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
            .when('/a', {
                templateUrl: 'adogs/a.dogs.html',
                controller: "aDogsController"
            })
            .when('/b', {
                templateUrl: 'bdogs/b.dogs.html',
                controller: "bDogsController"
            })
            .when('/c', {
                templateUrl: 'cdogs/c.dogs.html',
                controller: "cDogsController"
            })
            .when('/a/:dogId', {
                template: `<a-dogs-component dogs="dogs" dog-id="dogId"></a-dogs-component>`,
                controller: "aDogsController"
            })
            .when('/b/:dogId', {
                template: `<b-dogs-component dogs="dogs" dog-id="dogId"></b-dogs-component>`,
                controller: "bDogsController"
            })
            .when('/c/:dogId', {
                template: `<c-dogs-component dogs="dogs" dog-id="dogId"></c-dogs-component>`,
                controller: "cDogsController"
            })
            .otherwise({
                redirectTo: '/'
            })
        })