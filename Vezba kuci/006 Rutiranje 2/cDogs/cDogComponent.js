angular.module('myApp')
    .component('cDogsComponent', {
        template: `
        <h2>{{$ctrl.dogs[$ctrl.dogId].name}}</h2>
        <img ng-src="{{$ctrl.dogs[$ctrl.dogId].src}}">
        <p>{{$ctrl.dogs[$ctrl.dogId].snippet}}</p>
        `, 
        bindings: {
            dogId: "<",
            title: "<",
            dogs: "<"
        }
    })