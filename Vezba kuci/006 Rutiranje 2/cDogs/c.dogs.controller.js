angular.module('myApp')
.controller('cDogsController', function($scope,$routeParams){
    $scope.dogId = $routeParams.dogId;
    $scope.title= 'Dogs Starting With The Letter "C"';
    $scope.dogs = [
        {
            id: 0,
            name: "Canaan Dog",
            snippet: "One of the AKC's oldest breeds, the Canaan Dog is the national dog of Israel. This quick, medium-sized pasture dog is docile with family, aloof with strangers. The ever-alert Canaan is a vocal and persistent guardian of flock and home.",
            src: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12235112/Canaan-Dog-On-White-01.jpg"
        },
        {
            id: 1,
            name: "Cane Corso",
            snippet: "Smart, trainable, and of noble bearing, the assertive and confident Cane Corso is a peerless protector. The Corso’s lineage goes back to ancient Roman times, and the breed’s name roughly translates from the Latin as “bodyguard dog.”",
            src: "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/19112500/AKC-121516-296-800x600.jpg"
        }
    ];
    
})


