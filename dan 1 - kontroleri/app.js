var mojAngular = angular.module("mojAngular",[]);

mojAngular.controller("kontroler1",
function prvi($scope){
    $scope.ime = "Igor";
    $scope.prezime = "Marcikic";
    $scope.mesto = "Kovilj";

    $scope.objekti = [
        {
            slika: "https://bit.ly/2KHCqEN",
            naziv: "Slika 1"       
        },
        {
            slika: "https://bit.ly/2KHCqEN",
            naziv: "Slika 2" 
        },
        {
            slika: "https://bit.ly/2KHCqEN",
            naziv: "Slika 3" 
        }
    ];

    $scope.broj = 0;
    $scope.brojac = function(){
        $scope.broj ++;
    }
});

mojAngular.controller("kontroler2",
function drugi($scope){
    $scope.ja = {
        ime: "Igor",
        prezime: "Marcikic",
        godine: 30,
        email: "sadasdas@gmail.com"
    };

    $scope.submit = () => {
        var user = {
            ime: $scope.ja.ime,
            prezime: $scope.ja.prezime,
            godine: $scope.ja.godine,
            email: $scope.ja.email,

        };
        console.log(user.ime);
    }
});

