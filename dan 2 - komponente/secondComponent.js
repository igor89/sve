angular
.module("mojAngular")
.component("secondComponent", {
    templateUrl:"template2.html",
    controller: function firstComponentController($scope){
        $scope.ja = {
            ime: "Igor",
            prezime: "Marcikic",
            godine: 30,
            email: "sadasdas@gmail.com"
        };
    
        $scope.submit = () => {
            var user = {
                ime: $scope.ja.ime,
                prezime: $scope.ja.prezime,
                godine: $scope.ja.godine,
                email: $scope.ja.email,
    
            };
            console.log(user);
        }
    }
});