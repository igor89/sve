angular
.module("mojAngular")
.component("firstComponent", {
    templateUrl:"template1.html",
    controller: function firstComponentController($scope){
        $scope.ime = "Igor";
        $scope.prezime = "Marcikic";
        $scope.mesto = "Kovilj";
        $scope.query = '';
        $scope.objekti = [
            {
                slika: "https://bit.ly/2KHCqEN",
                opis: "Ovo je opis prve slike, broj 1.",
                naziv: "Prva slika"       
            },
            {
                slika: "https://vetstreet.brightspotcdn.com/dims4/default/5b3ffe7/2147483647/thumbnail/180x180/quality/90/?url=https%3A%2F%2Fvetstreet-brightspot.s3.amazonaws.com%2F8e%2F4e3910c36111e0bfca0050568d6ceb%2Ffile%2Fhub-dogs-puppy.jpg",
                opis: "Ovo je opis druge slike, broj 2.",
                naziv: "Druga slika" 
            },
            {
                slika: "https://www.petmd.com/sites/default/files/salmonella-infection-dogs.jpg",
                opis: "Ovo je opis trece slike, broj 3.",
                naziv: "Treca slika" 
            }
        ];
    }
});