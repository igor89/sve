angular.module('zadatak1')
    .component('ctrlOne', {
        templateUrl: 'ctrl1.html',
        bindings: {
            ime: '<',
            prezime: '<',
            mesto: '<',
            galerija: '<',
            a: '<',
            b: '<',
            g: '&'
        },
        controller: function CtrlOneController() {
            this.$onInit = () => {
                console.log(`this.mesto`);
                console.log(this.mesto);
                console.log(`this.a`);
                console.log(this.a);
                console.log(`this.b`);
                console.log(this.b);
            }
            this.saberi = function (a, b) {
                
                
                this.g({ a: a, b: b })
            }
        }
    });
