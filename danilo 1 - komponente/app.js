angular.module(`zadatak1`, [])
    .controller('KursCtrl',
        function KursCtrl() {
            //#region Variable
            this.ime = 'Danilo';
            this.prezime = 'Mogin';
            this.mesto = 'Novi Sad';

            this.a = 0;
            this.b = 0;

            this.galerija = [
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                },
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                },
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                },
                {
                    slika: 'https://img-9gag-fun.9cache.com/photo/aM23KQV_460s_v1.jpg',
                    naziv: 'Slika'
                }
            ];

            this.ja = {
                ime: 'Danilo',
                prezime: 'Mogin',
                godina: 33,
                email: 'danilo.mogin@gmail.com',
            }
            //#endregion

            this.send = () => {
                var userObj = {
                    ime: this.ja.ime,
                    prezime: this.ja.prezime,
                    godina: this.ja.godina,
                    email: this.ja.email,
                };
                console.log(`userObj`);
                console.log(userObj);
            }

            this.saberi = (a, b) => {
                console.log(`a + b`);
                console.log(a + b);

                return a + b;
            }
        }
    );
