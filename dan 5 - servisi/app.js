var myApp = angular.module('myApp', []); // injectuje se u ove uglaste zagrade ako je za neki drugi angular app

myApp.controller('CustomCtrl', function ControllerCtrl($scope, Dodavanje, Oduzimanje) {


    $scope.broj1 = 0;
    $scope.broj2 = 0;

    $scope.dodavanje = function () {
        $scope.rezultat = Dodavanje.sabiranjeBrojeva($scope.broj1, $scope.broj2);
    }

    $scope.broj3 = 0;
    $scope.broj4 = 0;

    $scope.oduzimanje = function () {
        $scope.rezultat2 = Oduzimanje.oduzimanjeBrojeva($scope.broj3, $scope.broj4);
    }

})

    .controller('GalerijaController',
        function GalerijaController($scope, $http) {
            $scope.getJson = $http.get('http://www.pgbovine.net/photos/json-files/boston.json').then(function (response) {
                $scope.BostonImages = response.data.images;
                console.log(`response`);
                console.log(response);
                console.log($scope.BostonImages);

            })
        })
        