angular.module(`myApp`, [])
    .controller('KursCtrl',
        function KursCtrl($scope) {
            //#region Variable
            $scope.ime = 'Danilo';
            $scope.prezime = 'Mogin';
            $scope.mesto = 'Novi Sad';

           

            $scope.ja = {
                ime: 'Danilo',
                prezime: 'Mogin',
                godina: 33,
                email: 'danilo.mogin@gmail.com',
            }
            //#endregion

            $scope.send = () => {
                var userObj = {
                    ime: $scope.ja.ime,
                    prezime: $scope.ja.prezime,
                    godina: $scope.ja.godina,
                    email: $scope.ja.email,
                };
                console.log(`userObj`);
                console.log(userObj);
            }
            
            $scope.a = 0;
            $scope.b = 0;

            $scope.saberi = (a, b) => {
                console.log(`a + b`);
                console.log(a + b);

                return a + b;
            }
        }
    );
