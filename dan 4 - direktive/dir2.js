angular.module('myApp')
    .directive('dirTwo', function() {
        return {
            restrict: "AE",
            scope: {
                a: "=",
                b:"=",
                saberi:"&onSaberi"
            },
        templateUrl: 'dir2.html',
    }


    // controller: function CtrlTwoController() {
    //     this.$onInit = () => {
    //         this.send();
    //     }
    //     this.send = function () {
    //         this.onSend({ ja: this.ja })
    //     }
    // }
});

// function createObjectFunction() {
//     console.log('Function Inside `createObjectFunction`:', this.foo);
//     return {
//         foo: 42,
//         bar: function () {
//             console.log('Function Inside `bar`:', this.foo);
//         },
//     };
// }

// function createObject() {
//     console.log('Arrow Inside `createObject`:', this.foo);
//     return {
//         foo: 42,
//         bar: () => console.log('Arrow Inside `bar`:', this.foo),
//     };
// }