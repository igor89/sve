angular.module('myApp')
    .config(function($routeProvider,$locationProvider){
        $locationProvider.hashPrefix('!');
        $routeProvider
        .when('/home', {
            templateUrl: './home/home.html',
            controller: 'HomeController'
        })
        .when('/phone', {
            templateUrl: './phone/phone.html',
            controller: 'PhoneController'
        })
        .when('/phones/:phoneId', { // : oznacava da je to parametar
            template: '<phone-details></phone-details>'
        }) 
        .otherwise({
            redirectTo: '/'
        })

    });