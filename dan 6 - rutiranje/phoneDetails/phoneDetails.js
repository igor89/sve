angular.module('myApp')
    .component('phoneDetails', {
        template:`
            Odabran je telefon sa ID: <span>{{$ctrl.phoneId}}</span> <br>
        `, // tu gore je $ctrl jer smo dole u funkciji stavili this
        controller: ['$routeParams',  // uglaste zagrade pokazu sta ima u funkciji, i za minifikaciju
        function PhoneDetailsController($routeParams){
                this.phoneId = $routeParams.phoneId; // phoneId je taj parametar koji se pozivau routes

            }
        ]
    })