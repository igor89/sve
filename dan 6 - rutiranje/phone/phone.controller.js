angular.module('myApp')
.controller('PhoneController', function($scope){
    $scope.title= 'Phone Page';
    $scope.phones = [
        {
            id: 1,
            name: "Prvi telefon",
            snippet: "Ovo je neki opis 1"
        },
        {
            id: 2,
            name: "Drugi telefon",
            snippet: "Ovo je neki opis 2"
        },
        {
            id: 3,
            name: "Treci telefon",
            snippet: "Ovo je neki opis 3"
        },
        {
            id: 4,
            name: "Cetvrti telefon",
            snippet: "Ovo je neki opis 4"
        }
    ];
})


